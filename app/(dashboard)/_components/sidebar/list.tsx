"use client";

import { useOrganizationList } from "@clerk/nextjs";
import Item from "./item";

const List = () => {
  const { userMemberships } = useOrganizationList({
    userMemberships: {
      infinite: true,
    },
  });

  const renderNavbar = () => {
    return userMemberships.data?.map((mem) => (
      <Item
        key={mem.organization.id}
        id={mem.organization.id}
        name={mem.organization.name}
        imageUrl={mem.organization.imageUrl}
      />
    ));
  };
  console.log("userMemberships", userMemberships);
  if (!userMemberships.data?.length) {
    return null;
  }
  return <ul className="space-y-4">{renderNavbar()}</ul>;
};

export default List;
