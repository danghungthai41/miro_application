export default {
  providers: [
    {
      domain: "https://wise-cricket-61.clerk.accounts.dev",
      applicationID: "convex",
    },
  ],
};
